import pytest
from model import Student, Teacher, Course, CourseSession, Quiz, Question, Answer


@pytest.fixture()
def student():
    return Student("Guillermina Véscovo")


@pytest.fixture()
def teacher():
    return Teacher("Jadwiga Of Poland")


@pytest.fixture()
def course():
    return Course("History 102")


@pytest.fixture()
def course_session():
    return CourseSession(Course("History 102"), Teacher("Jadwiga Of Poland"))


@pytest.fixture()
def another_course_session():
    return CourseSession(Course("History 102"), Teacher("Catherine The Great"))


@pytest.fixture()
def question1():
    return Question(
        "How many countries participated in WWI?",
        {"sentence": "10", "correct": False},
        {"sentence": "32", "correct": True},
        {"sentence": "2", "correct": False},
    )


@pytest.fixture()
def question2():
    return Question(
        "What's the biggest castle in the world?",
        {"sentence": "Prague Castle", "correct": True},
        {"sentence": "Windsor Castle", "correct": False},
        {"sentence": "Himeji Castle", "correct": False},
    )


@pytest.fixture()
def quiz():
    quiz = Quiz("History for beginners", Teacher("Jadwiga Of Poland"))
    quiz.teacher.quizzes.append(quiz)
    return quiz


def test_students_have_course_sessions(student):
    assert student.courses == list()


def test_students_have_quizzes(student):
    assert student.quizzes == list()


def test_students_can_enroll_and_drop_classes(student, course_session):
    student.enrol(course_session)
    assert len(student.courses) == 1
    assert len(course_session.students) == 1
    student.drop(course_session)
    assert len(student.courses) == 0
    assert len(course_session.students) == 0


def test_enrolling_saves_course_sessions_into_students_courses(student, course_session):
    student.enrol(course_session)
    assert type(student.courses[0]) == CourseSession


def test_student_cannot_enroll_twice_for_the_same_course_session(
    student, course_session
):
    student.enrol(course_session)
    student.enrol(course_session)
    assert len(student.courses) == 1


def test_student_cannot_drop_of_a_class_if_they_havent_registered_for_it(
    student, course_session, another_course_session
):
    student.enrol(course_session)
    assert len(student.courses) == 1

    student.drop(another_course_session)
    assert len(student.courses) == 1


def test_dropping_off_a_class_removes_the_exact_session(
    student, course_session, another_course_session
):
    student.enrol(course_session)
    student.enrol(another_course_session)

    assert len(student.courses) == 2
    assert course_session in student.courses
    assert another_course_session in student.courses

    student.drop(another_course_session)
    assert len(student.courses) == 1
    assert course_session in student.courses
    assert another_course_session not in student.courses


def test_enrolling_to_a_class_also_adds_the_student_to_the_sessions_students(
    student, course_session
):
    student.enrol(course_session)

    assert len(course_session.students) == 1
    assert student in course_session.students


def test_teacher_has_course_sessions(teacher):
    assert teacher.courses == list()


def test_teachers_can_teach_courses_by_creating_a_session(teacher, course):
    teacher.teach_course(course)
    assert len(teacher.courses) == 1
    assert type(teacher.courses[0]) == CourseSession


def test_teaching_a_course_stores_the_session_both_in_the_teacher_and_the_course(
    teacher, course
):
    teacher.teach_course(course)
    assert len(teacher.courses) == 1
    assert len(course.sessions) == 1
    assert teacher.courses[0] in course.sessions


def test_teachers_create_quizzes(teacher):
    assert len(teacher.quizzes) == 0
    teacher.create_quiz("Advanced")
    assert len(teacher.quizzes) == 1
    assert type(teacher.quizzes[0]) == Quiz


def test_teachers_can_assign_quizzes_to_students(student, quiz):
    assert len(quiz.students) == 0
    assert len(student.quizzes) == 0
    assert quiz.teacher.grades == {}

    quiz.teacher.assign_quiz(quiz, student)
    assert len(quiz.students) == 1
    assert len(student.quizzes) == 1
    assert student.name in quiz.teacher.grades
    assert quiz.teacher.grades[student.name] == []


def test_quizzes_cant_be_assigned_twice_to_the_same_student(student, quiz, capsys):
    quiz.teacher.assign_quiz(quiz, student)
    quiz.teacher.assign_quiz(quiz, student)
    captured = capsys.readouterr()
    assert captured.out == "The student has already been assigned that quiz.\n"


def test_teachers_cannot_assign_quizzes_from_other_teachers(
    teacher, student, quiz, capsys
):
    teacher.assign_quiz(quiz, student)
    captured = capsys.readouterr()
    assert captured.out == "This quiz is not yours to assign.\n"

    assert len(quiz.students) == 0
    assert len(student.quizzes) == 0
    assert student.name not in teacher.grades


def test_courses_have_sessions(course):
    assert course.sessions == list()


def test_course_sessions_belong_to_a_teacher_and_a_course(course_session):
    assert type(course_session.teacher) == Teacher
    assert type(course_session.course) == Course


def test_course_sessions_have_students_enrolled(course_session):
    assert course_session.students == list()


def test_quizzes_have_questions(quiz):
    assert quiz.questions == list()


def test_quizzes_belong_to_a_teacher(teacher):
    new_quiz = teacher.create_quiz("Advanced")
    assert new_quiz.teacher == teacher


def test_quizzes_have_students_who_were_assigned_the_quiz(quiz, student):
    quiz.teacher.assign_quiz(quiz, student)
    assert student in quiz.students
    assert type(quiz.students[0]) == Student


def test_questions_can_be_added_to_a_quiz(quiz, question1):
    assert len(quiz.questions) == 0
    quiz.add_question(question1)

    assert len(quiz.questions) == 1
    assert type(quiz.questions[0]) == Question
    assert quiz.questions[0].sentence == question1.sentence
    assert quiz.questions[0].options == question1.options


def test_questions_must_have_a_sentence(capsys):
    Question("", {})
    captured = capsys.readouterr()
    assert captured.out == "A question needs a statement.\n"


def test_a_question_must_have_at_least_3_options(capsys):
    Question("A question", {"sentence": "Empty question", "correct": False})
    captured = capsys.readouterr()
    assert captured.out == "There must be at least 3 options.\n"


def test_a_question_must_have_exactly_one_correct_answer(capsys):
    Question(
        "A question",
        {"sentence": "Empty question", "correct": True},
        {"sentence": "Empty question", "correct": True},
        {"sentence": "Empty question", "correct": False},
    )
    captured = capsys.readouterr()
    assert captured.out == "The question must have exactly 1 correct answer.\n"

    Question(
        "A question",
        {"sentence": "Empty question", "correct": False},
        {"sentence": "Empty question", "correct": False},
        {"sentence": "Empty question", "correct": False},
    )

    captured = capsys.readouterr()
    assert captured.out == "The question must have exactly 1 correct answer.\n"


def test_quizzes_can_have_many_questions(quiz, question1, question2):
    assert len(quiz.questions) == 0

    quiz.add_question(question1)
    quiz.add_question(question2)
    assert len(quiz.questions) == 2


def test_grading_a_question_returns_0_if_empty_invalid_or_incorrect_answer(question1):
    given_answer = ""
    assert question1.grade(given_answer) == 0

    given_answer = "10"
    assert question1.grade(given_answer) == 0

    given_answer = "invalid"
    assert question1.grade(given_answer) == 0


def test_grading_a_question_returns_1_if_correct_answer(question1):
    given_answer = "32"
    assert question1.grade(given_answer) == 1


def test_students_can_answer_questions(quiz, question1, student):
    quiz.add_question(question1)
    quiz.teacher.assign_quiz(quiz, student)
    assert student.responses == list()

    student.answer_question(quiz, quiz.questions[0], "10")
    assert len(student.responses) == 1
    assert type(student.responses[0]) == Answer
    assert student.responses[0].quiz == quiz
    assert student.responses[0].question == question1
    assert student.responses[0].selection == "10"


def test_answers_are_not_recorded_if_the_student_wasnt_assigned_the_quiz(
    quiz, question1, teacher, student, capsys
):
    quiz.add_question(question1)

    student.answer_question(quiz, quiz.questions[0], "10")
    assert len(student.responses) == 0
    captured = capsys.readouterr()
    assert captured.out == "You cannot take part in this quiz.\n"


def test_quizzes_can_be_graded_after_being_completed(
    quiz, question1, question2, student
):
    quiz.add_question(question1)
    quiz.add_question(question2)
    quiz.teacher.assign_quiz(quiz, student)

    student.answer_question(quiz, quiz.questions[0], "10")
    student.answer_question(quiz, quiz.questions[1], "Prague Castle")

    assert quiz.grade(student) == 0.5


def test_quizzes_cant_be_graded_by_other_teachers(
    quiz, question1, student, teacher, capsys
):
    quiz.add_question(question1)
    quiz.teacher.assign_quiz(quiz, student)

    student.answer_question(quiz, quiz.questions[0], "10")
    teacher.grade_quiz(quiz, student)
    captured = capsys.readouterr()
    assert captured.out == "That quiz is not yours to grade.\n"
    assert quiz.teacher.grades[student.name] == []


def test_each_graded_quiz_is_recorded_in_the_teachers_notebook(
    quiz, question1, student
):
    quiz.add_question(question1)
    quiz.teacher.assign_quiz(quiz, student)

    another_teacher = Teacher("Vlad The Impaler")
    another_quiz = another_teacher.create_quiz("History 101")
    another_quiz.add_question(question1)
    another_teacher.assign_quiz(another_quiz, student)

    student.answer_question(quiz, quiz.questions[0], "10")
    result = quiz.grade(student)
    assert student.name in quiz.teacher.grades
    assert quiz.teacher.grades[student.name] == [result]
    assert another_teacher.grades[student.name] == []


def test_grading_an_unfinished_quiz_raises_an_error(quiz, question1, student, capsys):
    quiz.add_question(question1)
    quiz.teacher.assign_quiz(quiz, student)
    quiz.grade(student)
    captured = capsys.readouterr()
    assert (
        captured.out
        == "Quizzes cannot be graded until all answers have been submitted.\n"
    )


def test_obtain_average_for_the_student_on_all_quizzes_by_a_certain_teacher(
    quiz, question1, student
):
    quiz.add_question(question1)
    quiz.teacher.assign_quiz(quiz, student)
    student.answer_question(quiz, quiz.questions[0], "10")
    result1 = quiz.grade(student)

    another_teacher = Teacher("Erik The Red")
    another_quiz = another_teacher.create_quiz("History 101")
    another_quiz.add_question(question1)
    another_teacher.assign_quiz(another_quiz, student)
    student.answer_question(another_quiz, another_quiz.questions[0], "32")
    result2 = another_quiz.grade(student)

    assert quiz.teacher.get_average(student) == result1 / 1


def test_averages_cannot_be_calculated_if_the_student_wasnt_assigned_any_quizzes(
    quiz, student, capsys
):
    quiz.grade(student)
    captured = capsys.readouterr()
    assert captured.out == "That student hasn't been assigned this quiz.\n"
