@student @drop
Feature: Dropping a class
  As a student, I want to drop a class.

  Background:
    Given a course named "History 102" exists
    And at least one Teacher exists
    And a CourseSession exists for "History 102"
    And a Student exists

  Scenario:
    Given the student is enrolled in the course
    When the desired CourseSession to drop is selected
    Then the Student is dropped from it
    And the CourseSession no longer has the student on it.

  Scenario: Dropping a class I'm not registered for
    Given the student is not enrolled in the course
    When the desired CourseSession to drop is selected
    Then the student is not dropped.

  Scenario: Dropping a class that doesn't exist
    Given the student is not enrolled in the course
    When the non existent CourseSession is selected
    Then the student is not dropped.
