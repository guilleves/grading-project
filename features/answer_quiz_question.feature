@answer @quiz @student
Feature: Answer a question from a Quiz
  As a Student, I want to answer a question from a quiz.

  Background:
    Given a Student exists

  Scenario: Answering a question from my quiz
    Given at least one Teacher exists
    And a Quiz by that teacher exists
    And the Quiz has questions
    And the Student already has the quiz assigned
    When the question is answered
    Then the response is recorded


  Scenario: Answering a question from my quiz that has already been answered
    Given at least one Teacher exists
    And a Quiz by that teacher exists
    And the Quiz has questions
    And the Student already has the quiz assigned
    And the Student has answered the question
    When the question is answered with a different value
    Then the response is not recorded


  Scenario: Answering a question from my quiz with an invalid choice
    Given at least one Teacher exists
    And a Quiz by that teacher exists
    And the Quiz has questions
    And the Student already has the quiz assigned
    When the question is answered with an invalid option
    Then the response is recorded
    And the response has an incorrect value


  Scenario: Answering a question from a quiz I've not been asigned
    Given a quiz by another Teacher exists
    And the Quiz has questions
    And the student doesn't have that quiz assigned
    When the question is answered
    Then the response is not recorded
