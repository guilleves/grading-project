from behave import *
from model import Student, Quiz, Question, Answer


@given("the Quiz has questions")
def step_impl(context):
    context.question = Question(
        "How many wives did Henry VIII have?",
        {"sentence": "5", "correct": False},
        {"sentence": "6", "correct": True},
        {"sentence": "7", "correct": False},
    )
    context.quiz.add_question(context.question)


@given("the Student has answered the question")
def step_impl(context):
    context.answer = context.student.answer_question(
        context.quiz, context.question, "6"
    )


@given("the student doesn't have that quiz assigned")
def step_impl(context):
    assert context.quiz not in context.student.quizzes


@when("the question is answered")
def step_impl(context):
    context.answer = context.student.answer_question(
        context.quiz, context.question, "6"
    )


@when("the question is answered with a different value")
def step_impl(context):
    context.answer = context.student.answer_question(
        context.quiz, context.question, "5"
    )


@when("the question is answered with an invalid option")
def step_impl(context):
    context.answer = context.student.answer_question(
        context.quiz, context.question, "invalid"
    )


@then("the response is recorded")
def step_impl(context):
    assert context.answer in context.student.responses


@then("the response is not recorded")
def step_impl(context):
    assert context.answer not in context.student.responses


@then("the response has an incorrect value")
def step_impl(context):
    incorrect_value = [
        option["sentence"]
        for option in context.answer.question.options
        if option["correct"]
    ][0]

    assert incorrect_value == context.answer.selection
