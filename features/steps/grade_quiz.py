from behave import *
from model import Student, Quiz, Teacher, Question, Answer


@given("the Quiz has more than one question")
def step_impl(context):
    context.second_question = Question(
        "At what age did Cleopatra committed suicide?",
        {"sentence": "35", "correct": False},
        {"sentence": "39", "correct": True},
        {"sentence": "44", "correct": False},
    )
    context.quiz.add_question(context.second_question)


@given("the Student has completed the quiz")
def step_impl(context):
    context.answer = context.student.answer_question(
        context.quiz, context.question, "6"
    )


@given("the Student hasn't completed the quiz")
def step_impl(context):
    context.answer = context.student.answer_question(
        context.quiz, context.question, "6"
    )


@when("the quiz is graded")
def step_impl(context):
    context.quiz_grade = context.teacher.grade_quiz(context.quiz, context.student)


@then("the quiz results are saved on the teacher")
def step_impl(context):
    assert context.teacher.grades[context.student.name] == [context.quiz_grade]


@then("the quiz results aren't saved on the teacher")
def step_impl(context):
    assert context.teacher.grades[context.student.name] == []


@then("the quiz results aren't recorded")
def step_impl(context):
    assert context.quiz_grade == None
