from behave import *
from model import Quiz, Teacher, Question


@given("a Quiz exists")
def step_impl(context):
    context.quiz = Quiz("Romance in the Middle Ages", Teacher("Philip The Handsome"))


@given("the question is valid")
def step_impl(context):
    context.question = Question(
        "How many wives did Henry VIII have?",
        {"sentence": "5", "correct": False},
        {"sentence": "6", "correct": True},
        {"sentence": "7", "correct": False},
    )


@given("the question has less than three choices")
def step_impl(context):
    context.question = Question(
        "How many wives did Henry VIII have?",
        {"sentence": "6", "correct": True},
        {"sentence": "7", "correct": False},
    )


@given("the question doesn't have a sentence")
def step_impl(context):
    context.question = Question(
        "",
        {"sentence": "5", "correct": False},
        {"sentence": "6", "correct": True},
        {"sentence": "7", "correct": False},
    )


@given("the question has more than one correct answer")
def step_impl(context):
    context.question = Question(
        "How many wives did Henry VIII have?",
        {"sentence": "5", "correct": True},
        {"sentence": "6", "correct": True},
        {"sentence": "7", "correct": False},
    )


@given("the question has no correct answer")
def step_impl(context):
    context.question = Question(
        "How many wives did Henry VIII have?",
        {"sentence": "5", "correct": False},
        {"sentence": "6", "correct": False},
        {"sentence": "7", "correct": False},
    )


@when("the question is created")
def step_impl(context):
    context.quiz.add_question(context.question)


@then("a sentence is assigned to the question")
def step_impl(context):
    assert context.question.sentence == "How many wives did Henry VIII have?"


@then("the sentence has three options")
def step_impl(context):
    assert len(context.question.options) == 3


@then("one of the options is correct")
def step_impl(context):
    correct_answers = 0
    for option in context.question.options:
        if option["correct"]:
            correct_answers += 1
    assert correct_answers == 1


@then("the question is in the quiz")
def step_impl(context):
    assert context.question in context.quiz.questions


@then("no question is saved")
def step_impl(context):
    assert hasattr(context.question, "sentence") is False
