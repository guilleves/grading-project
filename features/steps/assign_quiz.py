from behave import *
from model import Student, Teacher, Quiz


@given("a Quiz by that teacher exists")
def step_impl(context):
    context.quiz = context.teacher.create_quiz("History of the Ustaše")


@given("the Student already has the quiz assigned")
def step_impl(context):
    context.teacher.assign_quiz(context.quiz, context.student)


@given("the quiz doesn't exist")
def step_impl(context):
    context.quiz = None


@given("the student doesn't exist")
def step_impl(context):
    context.student = None


@given("a quiz by another Teacher exists")
def step_impl(context):
    context.quiz = Quiz("How to build an empire by 23", Teacher("Alexander The Great"))


@when("the quiz is assigned")
def step_impl(context):
    context.teacher.assign_quiz(context.quiz, context.student)


@then("the quiz has the student in it")
def step_impl(context):
    assert context.student in context.quiz.students


@then("the student has the quiz in it")
def step_impl(context):
    assert context.quiz in context.student.quizzes


@then("the length of the student's quizzes is not changed")
def step_impl(context):
    assert len(context.student.quizzes) == 1


@then("the length of the quiz's students is not changed")
def step_impl(context):
    assert len(context.quiz.students) == 1


@then("no quiz is saved in the student.")
def step_impl(context):
    assert len(context.student.quizzes) == 0


@then("no student is saved in the quiz.")
def step_impl(context):
    assert len(context.quiz.students) == 0
