from behave import *
from model import Student, Quiz, Teacher, Question, Answer


@given("all their completed quizzes have been graded")
def step_impl(context):
    for quiz in context.student.quizzes:
        quiz.grade(context.student)


@given("the Student has completed another teacher's quiz")
def step_impl(context):
    context.another_teacher = Teacher("Alexander The Great")
    another_quiz = context.another_teacher.create_quiz("How to build an empire by 23")
    another_question = Question(
        "Who has the Koh-I-Noor now?",
        {"sentence": "The British Royal family", "correct": True},
        {"sentence": "The Indian Empress", "correct": False},
        {"sentence": "The Louvre Museum", "correct": False},
    )
    another_quiz.add_question(another_question)
    context.another_teacher.assign_quiz(another_quiz, context.student)
    context.student.answer_question(
        another_quiz, another_question, "The Indian Empress"
    )


@when("the average is calculated")
def step_impl(context):
    context.student_average = context.teacher.get_average(context.student)


@then("the result is obtained")
def step_impl(context):
    assert type(context.student_average) == float


@then("no result is obtained")
def step_impl(context):
    assert context.student_average == None


@then("the result corresponds only to the grading teacher")
def step_impl(context):
    assert len(context.teacher.grades[context.student.name]) == 1
    assert len(context.another_teacher.grades[context.student.name]) == 1
    assert context.student_average == 1
