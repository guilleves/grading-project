from behave import *
from model import Student, Teacher, Course, CourseSession


@given("the teacher is not teaching the course")
def step_impl(context):
    assert context.teacher.courses == list()


@when("the desired Course to teach is selected")
def step_impl(context):
    context.course_session = context.teacher.teach_course(context.course)


@when("the non existent Course to teach is selected")
def step_impl(context):
    context.course = None


@then("the CourseSession is assigned to the Teacher")
def step_impl(context):
    assert len(context.teacher.courses) == 1
    assert context.course_session in context.teacher.courses


@then("the CourseSession has the theacher in it.")
def step_impl(context):
    assert context.teacher == context.course_session.teacher


@given("the teacher is already teaching the course")
def step_impl(context):
    context.course_session = context.teacher.teach_course(context.course)
    assert context.course_session in context.teacher.courses


@then("the CourseSession is not assigned to the Teacher again.")
def step_impl(context):
    assert len(context.teacher.courses) == 1


@then("the CourseSession is not assigned to the Teacher.")
def step_impl(context):
    assert len(context.teacher.courses) == 0
