from behave import *
from model import Quiz, Teacher, Question


@given("the student has answered the question correctly")
def step_impl(context):
    context.answer = context.student.answer_question(
        context.quiz, context.question, "6"
    )


@given("the student has answered the question incorrectly")
def step_impl(context):
    context.answer = context.student.answer_question(
        context.quiz, context.question, "5"
    )


@when("the answer is graded")
def step_impl(context):
    context.grade = context.question.grade(context.answer.selection)


@then("a grade of 1 is returned")
def step_impl(context):
    assert context.grade == 1


@then("a grade of 0 is returned")
def step_impl(context):
    assert context.grade == 0
