from behave import *
from model import Student, Teacher, Course, CourseSession, Quiz, Question, Answer


@given('a course named "History 102" exists')
def step_impl(context):
    context.course = Course("History 102")


@given("at least one Teacher exists")
def step_impl(context):
    context.teacher = Teacher("Ivan The Terrible")


@given('a CourseSession exists for "History 102"')
def step_impl(context):
    context.course_session = CourseSession(context.course, context.teacher)


@given("a Student exists")
def step_impl(context):
    context.student = Student("Guillermina Véscovo")


@given("the student is not registered for the course")
def step_impl(context):
    assert context.course_session not in context.student.courses


@when("the desired CourseSession is selected")
def step_impl(context):
    context.student.enrol(context.course_session)


@then("the Student is enrolled in it")
def step_impl(context):
    assert context.course_session in context.student.courses


@then("the CourseSession has a new student registered.")
def step_impl(context):
    assert context.student in context.course_session.students


@given("the student has already enrolled in the class")
def step_impl(context):
    context.student.enrol(context.course_session)
    assert context.course_session in context.student.courses


@then("the student is not enrolled again.")
def step_impl(context):
    assert context.course_session in context.student.courses
    assert len(context.student.courses) == 1
    assert len(context.course_session.students) == 1


@when("the non existent CourseSession is selected")
def step_impl(context):
    context.course_session = None


@then("the student is not enrolled.")
def step_impl(context):
    assert context.course_session not in context.student.courses
    assert len(context.student.courses) == 0
