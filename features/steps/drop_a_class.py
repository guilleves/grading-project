from behave import *
from model import Student, Teacher, Course, CourseSession


@given("the student is enrolled in the course")
def step_impl(context):
    context.student.enrol(context.course_session)
    assert context.course_session in context.student.courses


@when("the desired CourseSession to drop is selected")
def step_impl(context):
    context.student.drop(context.course_session)


@then("the Student is dropped from it")
def step_impl(context):
    assert context.course_session not in context.student.courses


@then("the CourseSession no longer has the student on it.")
def step_impl(context):
    assert context.student not in context.course_session.students


@given("the student is not enrolled in the course")
def step_impl(context):
    assert context.course_session not in context.student.courses


@then("the student is not dropped.")
def step_impl(context):
    assert context.course_session not in context.student.courses
    assert len(context.student.courses) == 0
