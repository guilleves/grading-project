from behave import *
from model import Student, Teacher, Quiz


@when("the quiz is created")
def step_impl(context):
    context.quiz = context.teacher.create_quiz("History of the Ustaše")


@then("a name is assigned to the quiz")
def step_impl(context):
    assert context.quiz.name == "History of the Ustaše"


@then("the quiz has the teacher in it")
def step_impl(context):
    assert context.teacher == context.quiz.teacher


@then("the Teacher has the quiz in it.")
def step_impl(context):
    assert context.quiz in context.teacher.quizzes


@when("the quiz is attempted to be created")
def step_impl(context):
    context.quiz = Quiz("Trying to cheat?", Student("A cheater"))


@then("nothing is saved.")
def step_impl(context):
    assert hasattr(context.quiz, "teacher") is False
