@quiz
Feature: Grade a quiz as a whole
  As a Teacher, I want to grade a given quiz for a certain student

  Background:
    Given at least one Teacher exists
    And a Quiz by that teacher exists
    And the Quiz has questions
    And a Student exists

  Scenario: Grading a complete quiz
    Given the Student already has the quiz assigned
    And the Student has completed the quiz
    When the quiz is graded
    Then the quiz results are saved on the teacher

  Scenario: Grading an incomplete quiz
    Given the Quiz has more than one question
    And the Student already has the quiz assigned
    And the Student hasn't completed the quiz
    When the quiz is graded
    Then the quiz results aren't saved on the teacher

  Scenario: Grading a quiz that doesn't belong to me
    Given a quiz by another Teacher exists
    When the quiz is graded
    Then the quiz results aren't recorded

  Scenario: Grading a quiz that wasn't assigned to the student
    Given the Student has answered the question
    When the quiz is graded
    Then the quiz results aren't recorded
