@quiz @grade @average
Feature: Calculate average per student, per teacher
  As a Teacher, I want to obtain the average grade a Student got across all my classes.

  Background:
    Given at least one Teacher exists
    And a Quiz by that teacher exists
    And the Quiz has questions
    And a Student exists

  Scenario: Obtaining average for a student who has taken my quizzes
    Given the Student already has the quiz assigned
    And the Student has completed the quiz
    And all their completed quizzes have been graded
    When the average is calculated
    Then the result is obtained

  Scenario: Obtaining average for a student who has also taken other teachers' quizzes
    Given the Student already has the quiz assigned
    And the Student has completed the quiz
    And the Student has completed another teacher's quiz
    And all their completed quizzes have been graded
    When the average is calculated
    Then the result is obtained
    And the result corresponds only to the grading teacher

  Scenario: Obtaining average for a student who hasn't been assigned any quizzes
    When the average is calculated
    Then no result is obtained

  Scenario: Obtaining average for a student who hasn't completed any quizzes yet
    When the average is calculated
    Then no result is obtained
