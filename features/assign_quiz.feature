@quiz @create @assign
Feature: Create a Quiz
  As a Teacher, I want to assign a quiz to a student.

  Background:
    Given at least one Teacher exists
    And a Quiz by that teacher exists

  Scenario: Assigning a quiz to an existing student
    Given a Student exists
    When the quiz is assigned
    Then the student has the quiz in it
    And the quiz has the student in it


  Scenario: Assigning a quiz to an existing student
    Given a Student exists
    And the Student already has the quiz assigned
    When the quiz is assigned
    Then the length of the student's quizzes is not changed
    And the length of the quiz's students is not changed


  Scenario: Assigning a quiz to an invalid student
    Given the student doesn't exist
    When the quiz is assigned
    Then no student is saved in the quiz.


  Scenario: Assigning a non existent quiz to an existing student
    Given a Student exists
    Given the quiz doesn't exist
    When the quiz is assigned
    Then no quiz is saved in the student.

  Scenario: Assigning a quiz that doesn't belong to me
    Given a Student exists
    And a quiz by another Teacher exists
    When the quiz is assigned
    Then no student is saved in the quiz.
    And no quiz is saved in the student.
