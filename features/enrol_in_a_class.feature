@student @enroll
Feature: Enrolling in a class
  As a student, I want to enrol in a class taught by any Professor.

  Background:
    Given a course named "History 102" exists
    And at least one Teacher exists
    And a CourseSession exists for "History 102"
    And a Student exists

  Scenario: Enrolling in a new class
    Given the student is not registered for the course
    When the desired CourseSession is selected
    Then the Student is enrolled in it
    And the CourseSession has a new student registered.

  Scenario: Enrolling in a class I'm already registered for
    Given the student has already enrolled in the class
     When the desired CourseSession is selected
     Then the student is not enrolled again.

   Scenario: Enrolling in a class that doesn't exist
      Given the student is not registered for the course
      When the non existent CourseSession is selected
      Then the student is not enrolled.
