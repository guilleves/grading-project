@quiz @create @assign
Feature: Create a Quiz
  As a Teacher, I want to create a quiz.

  Background:
    Given at least one Teacher exists

  Scenario:
    When the quiz is created
    Then a name is assigned to the quiz
    And the quiz has the teacher in it
    And the Teacher has the quiz in it.

  Scenario: Creating a Quiz with an invalid Teacher
    When the quiz is attempted to be created
    Then nothing is saved.
