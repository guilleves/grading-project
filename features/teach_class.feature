@teacher @course
Feature: Teaching a class
  As a teacher, I want to teach a class.

  Background:
    Given a course named "History 102" exists
    And at least one Teacher exists

  Scenario: Teaching a new course
    Given the teacher is not teaching the course
    When the desired Course to teach is selected
    Then the CourseSession is assigned to the Teacher
    And the CourseSession has the theacher in it.

  Scenario: Teaching a course I'm already teaching
    Given the teacher is already teaching the course
    When the desired Course to teach is selected
    Then the CourseSession is not assigned to the Teacher again.


  Scenario: Teaching a course that doesn't exist
    Given the teacher is not teaching the course
    When the non existent Course to teach is selected
    Then the CourseSession is not assigned to the Teacher.
