@questions @create
Feature: Add question to a quiz
  As a Teacher, I want to add questions to an already existing quiz.

  Background:
    Given a Quiz exists

  Scenario: Creating a valid question
    Given the question is valid
    When the question is created
    Then a sentence is assigned to the question
    And the sentence has three options
    And one of the options is correct
    And the question is in the quiz

  Scenario: Creating a question with less than three choices
    Given the question has less than three choices
    When the question is created
    Then no question is saved

  Scenario: Creating a question without a sentence
    Given the question doesn't have a sentence
    When the question is created
    Then no question is saved

  Scenario: Creating a question with more than one correct answer
    Given the question has more than one correct answer
    When the question is created
    Then no question is saved

  Scenario: Creating a question with no correct answer
    Given the question has no correct answer
    When the question is created
    Then no question is saved
