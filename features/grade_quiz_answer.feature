@answer @quiz @student
Feature: Grade a given answer
  As a Question, I want to grade the answer given by a Student.

  Background:
    Given at least one Teacher exists
    And a Quiz by that teacher exists
    And the Quiz has questions
    Given a Student exists
    And the Student already has the quiz assigned

  Scenario: Grading a correct answer
    Given the student has answered the question correctly
    When the answer is graded
    Then a grade of 1 is returned

  Scenario: Grading an incorrect answer
    Given the student has answered the question incorrectly
    When the answer is graded
    Then a grade of 0 is returned
