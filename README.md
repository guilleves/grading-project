# Grading project

### For the sake of simplicity (and time), I've made a couple of assumptions regarding the domain of the problem.

- Courses can be taught by different teachers.
- Every combination of Course + Teacher gives us a `CourseSession`, which is an offering for the course.
- Students enrol for sessions of a course.
- Quizzes don't belong to a CourseSession, but to a Teacher.
- Quizzes cannot be graded until all questions have been answered.
- Unanswered questions are not registered -- one option must have been selected.
- Questions must have at least 3 options.
- Each correct answer gives 1 point, while each incorrect answers gives 0 points.
- The quiz grade is calculated by the average for all questions on it.
- There's no concept of time.

Due to lack of correlation between a `Quiz` and a `CourseSession`, averages cannot be calculated by Course and Student, since the student can solve n-quizzes by the same teacher in different courses and we'll never know to which class that quiz was supposed to belong.

This means we can only obtain the average grade a `Student` got for the quizzes assigned by a certain `Teacher`.

To calculate the average per student, each of their quizzes must be graded by the teacher. For example:
```python
teacher.grade_quiz(quiz1, student)
teacher.grade_quiz(quiz2, student)

teacher.get_average(student)
```

### The test suite is run with pytest and behave

```sh
py.test spec/main.py -vs
```

```sh
========================= test session starts =========================
platform linux -- Python 3.7.2, pytest-4.2.0, py-1.7.0, pluggy-0.8.1
rootdir: /home/guillermina/grading-project, inifile:
collected 36 items                                                                                                                                   

spec/main.py ....................................                 [100%]

====================== 36 passed in 0.28 seconds =======================
```

```sh
behave
```

```
10 features passed, 0 failed, 0 skipped
35 scenarios passed, 0 failed, 0 skipped
230 steps passed, 0 failed, 0 skipped, 0 undefined
Took 0m0.022s
```
