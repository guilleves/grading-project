class Teacher:
    def __init__(self, name):
        self.name = name
        self.courses = list()
        self.quizzes = list()
        self.grades = {}

    def teach_course(self, course):
        if type(course) != Course:
            print("Invalid course")
            return
        for session in self.courses:
            if session.course.name == course.name:
                print("This teacher is already teaching that Course")
                return
        session = CourseSession(course, self)
        self.courses.append(session)
        course.sessions.append(session)
        return session

    def create_quiz(self, quiz_name):
        new_quiz = Quiz(quiz_name, self)
        self.quizzes.append(new_quiz)
        return new_quiz

    def assign_quiz(self, quiz, student):
        if type(student) != Student:
            print("Invalid student.")
            return
        if type(quiz) != Quiz:
            print("Invalid quiz.")
            return
        if quiz in student.quizzes:
            print("The student has already been assigned that quiz.")
            return
        if quiz not in self.quizzes:
            print("This quiz is not yours to assign.")
            return
        student.quizzes.append(quiz)
        quiz.students.append(student)
        if student.name not in self.grades:
            self.grades[student.name] = []
            quiz.teacher = self

    def grade_quiz(self, quiz, student):
        if quiz not in self.quizzes:
            print("That quiz is not yours to grade.")
            return
        return quiz.grade(student)

    def get_average(self, student):
        if student.name not in self.grades:
            print("That student wasn't assigned any quizzes.")
            return

        student_grades = self.grades[student.name]
        if len(student_grades) == 0:
            print("This student hasn't completed any quizzes yet.")
        return sum(student_grades) / float(len(student_grades))


class Student:
    def __init__(self, name):
        self.name = name
        self.courses = list()
        self.quizzes = list()
        self.responses = list()

    def enrol(self, course_session):
        if course_session in self.courses:
            print("This student is already enrolled in that class!")
            return
        else:
            self.courses.append(course_session)
            course_session.students.append(self)

    def drop(self, course_session):
        if course_session not in self.courses:
            print("This student is not enrolled in the class.")
            return
        else:
            self.courses.remove(course_session)
            course_session.students.remove(self)

    def answer_question(self, quiz, question, given_answer):
        if quiz not in self.quizzes:
            print("You cannot take part in this quiz.")
            return
        else:
            response = Answer(self, quiz, question, given_answer)
            for r in self.responses:
                if r.quiz == quiz and r.question == question:
                    print("That question has already been answered")
                    return

            self.responses.append(response)
            return response

    def get_answers_by_quiz(self, quiz):
        if quiz not in self.quizzes:
            print("This student hasn't taken the quiz.")
            return
        else:
            answers = list()
            for response in self.responses:
                if response.quiz == quiz:
                    answers.append(response)
            return answers


class Course:
    def __init__(self, name):
        self.name = name
        self.sessions = list()


class CourseSession:
    def __init__(self, course, teacher):
        self.course = course
        self.teacher = teacher
        self.students = list()


class Quiz:
    def __init__(self, name, teacher):
        if not type(teacher) == Teacher:
            print("The Teacher is not valid.")
            return

        self.name = name
        self.teacher = teacher
        self.questions = list()
        self.students = list()

    def add_question(self, question):
        self.questions.append(question)

    def grade(self, student):
        if self not in student.quizzes:
            print("That student hasn't been assigned this quiz.")
            return
        student_responses = student.get_answers_by_quiz(self)
        if not len(student_responses) == len(self.questions):
            print("Quizzes cannot be graded until all answers have been submitted.")
            return
        else:
            total = 0
            for answer in student_responses:
                grade = answer.question.grade(answer.selection)
                self.teacher.grades[student.name].append(grade)
                total += grade
            return total / len(student_responses)


class Question:
    def __init__(self, sentence, *options):
        if not sentence:
            print("A question needs a statement.")
            return
        if not options or len(options) < 3:
            print("There must be at least 3 options.")
            return

        correct_answers = 0
        for o in options:
            if o["correct"] is True:
                correct_answers += 1
        if correct_answers != 1:
            print("The question must have exactly 1 correct answer.")
            return

        self.sentence = sentence
        self.options = options

    def grade(self, selection):
        for option in self.options:
            if option["sentence"] == selection and option["correct"]:
                return 1
        return 0


class Answer:
    def __init__(self, student, quiz, question, selection):
        if not type(question) == Question:
            print("The answer must belong to a question.")
            return

        self.question = question
        self.quiz = quiz
        self.student = student
        if not selection.strip():
            print("The answer cannot be blank.")
            return

        self.selection = selection
        incorrect_value = [
            option["sentence"] for option in question.options if option["correct"]
        ][0]
        for option in self.question.options:
            if option["sentence"] == selection:
                self.selection = selection
                break
            else:
                self.selection = incorrect_value
